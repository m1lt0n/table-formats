# Table Formats [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/m1lt0n%2Ftable-formats/HEAD)

A collection of [Jupyter](http://jupyter.org/) notebooks about using table formats with Spark.

## Run interactively on Binder

The easiest way to get started is to simply open this link in your browser: [Binder](https://mybinder.org/v2/gl/m1lt0n%2Ftable-formats/HEAD). that's all, you only need your browser!

## Running locally

To run these notebooks locally:
1. Install [Jupyter Notebook](http://jupyter.org/install) or [JupyterLab](https://jupyterlab.readthedocs.io/en/stable/)
2. Install an [Almond kernel](https://almond.sh/docs/quick-start-install)
3. Clone the project and run `jupyter lab` in the project directory
4. Open one of the notebooks and play with it!
