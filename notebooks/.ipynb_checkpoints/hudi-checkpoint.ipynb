{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Iceberg with Spark\n",
    "\n",
    "## Setting up\n",
    "\n",
    "Import Spark 3.0.3 with almond-spark integration & Iceberg runtime 0.12.1."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "import $ivy.`org.apache.spark::spark-sql:3.0.3`\n",
    "// import $ivy.`sh.almond::almond-spark:_` done automatically\n",
    "import $ivy.`org.apache.iceberg:iceberg-spark3-runtime:0.12.1`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Disable logging in order to avoid polluting your cell outputs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "import org.apache.log4j.{Level, Logger}\n",
    "Logger.getLogger(\"org\").setLevel(Level.OFF)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Create a `SparkSession` using the `NotebookSparkSessionBuilder` provided by almond-spark, and enable Iceberg support. This is going to be running in local mode (ie same JVM as the kernel). We will also create a local catalog named \"icebergcatalog\" for Iceberg tables at the path data/iceberg_data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "import org.apache.spark.sql._\n",
    "\n",
    "val spark = {\n",
    "  NotebookSparkSession.builder()\n",
    "    .config(\"spark.sql.extensions\", \"org.apache.iceberg.spark.extensions.IcebergSparkSessionExtensions\")\n",
    "    .config(\"spark.sql.catalog.spark_catalog\", \"org.apache.iceberg.spark.SparkSessionCatalog\")\n",
    "    .config(\"spark.sql.catalog.spark_catalog.type\", \"hive\")\n",
    "    .config(\"spark.sql.catalog.icebergcatalog\", \"org.apache.iceberg.spark.SparkCatalog\")\n",
    "    .config(\"spark.sql.catalog.icebergcatalog.type\", \"hadoop\")\n",
    "    .config(\"spark.sql.catalog.icebergcatalog.warehouse\", \"data/iceberg_data\")\n",
    "    .master(\"local[*]\")\n",
    "    .getOrCreate()\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When running this, you should see that the cell output contains a link to the Spark UI. This will only work if you have connectivity to the environment (that is, running locally); it will not work in binder."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Testing Spark\n",
    "\n",
    "Now that we have a `SparkSession`, let's test RDD & DF/DS computations and visualizations.\n",
    "\n",
    "### RDDs\n",
    "\n",
    "When you execute a Spark action like `sum` you should see a progress bar, showing the progress of the running Spark job. If you're using the Jupyter classic UI, you can also click on *(kill)* to cancel the job."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "def sc = spark.sparkContext\n",
    "val rdd = sc.parallelize(1 to 100000000, 100)\n",
    "val x = rdd.map(_ + 1).sum()\n",
    "val y = rdd.map(n => (n % 10, n)).reduceByKey(_ + _).collect()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Datasets and Dataframes\n",
    "\n",
    "If you try to create a `Dataset` or a `Dataframe` from some data structure containing a case class and you're getting an `org.apache.spark.sql.AnalysisException: Unable to generate an encoder for inner class ...` when calling `.toDS`/`.toDF`, try the following workaround:\n",
    "\n",
    "Add `org.apache.spark.sql.catalyst.encoders.OuterScopes.addOuterScope(this)` in the same cell where you define case classes involved."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "import spark.implicits._\n",
    "org.apache.spark.sql.catalyst.encoders.OuterScopes.addOuterScope(this);\n",
    "case class Person(id: Long, name: String)\n",
    "val people = List(Person(42, \"Alice\"), Person(43, \"Bob\"), Person(44, \"Charlie\")).toDS\n",
    "people.registerTempTable(\"people\")\n",
    "people.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Playing with Iceberg\n",
    "\n",
    "### Creating a table\n",
    "\n",
    "To create your first Iceberg table in Spark, use the spark-sql shell or spark.sql(...) to run a CREATE TABLE command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "spark.sql(\"CREATE TABLE icebergcatalog.db.people (id bigint, name string) USING iceberg\")\n",
    "spark.sql(\"describe table icebergcatalog.db.people\").show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Writing\n",
    "\n",
    "Once your table is created, insert data using INSERT INTO:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "spark.sql(\"INSERT INTO icebergcatalog.db.people VALUES (32, 'Mark'), (33, 'John'), (34, 'Lucy')\") // insert some persons\n",
    "spark.sql(\"INSERT INTO icebergcatalog.db.people SELECT id, name FROM people WHERE length(name) = 3\") // adds Bob from the previous example"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Iceberg also adds row-level SQL updates to Spark, MERGE INTO and DELETE FROM:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "spark.sql(\"\"\"MERGE INTO icebergcatalog.db.target t USING (SELECT * FROM updates) u ON t.id = u.id\n",
    "WHEN MATCHED THEN UPDATE SET t.count = t.count + u.count\n",
    "WHEN NOT MATCHED THEN INSERT *\"\"\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Iceberg supports writing DataFrames using the new v2 DataFrame write API:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "spark.table(\"people\")\n",
    "    .select(\"id\", \"name\")\n",
    "    .writeTo(\"icebergcatalog.db.people\")\n",
    "    .append() // adds everybody from previous example"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Reading\n",
    "\n",
    "To read with SQL, use the an Iceberg table name in a SELECT query:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "spark.sql(\"SELECT count(1) as count, name FROM icebergcatalog.db.people GROUP BY name\").show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "SQL is also the recommended way to inspect tables. To view all of the snapshots in a table, use the snapshots metadata table:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "spark.sql(\"SELECT * FROM icebergcatalog.db.table.snapshots\").show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "DataFrame reads are supported and can reference tables by name using spark.table:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "val df = spark.table(\"icebergcatalog.db.people\")\n",
    "df.count()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise\n",
    "\n",
    "Create Iceberg tables for tables \"data\" and \"data_deltas\". Table \"data\" contains the rows, \"data_deltas\" contains changes to \"data\" that need to be applied to it.\n",
    "\n",
    "Using the SQL MERGE INTO statement, merge all deltas from \"data_deltas\" into \"data\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Scala",
   "language": "scala",
   "name": "scala"
  },
  "language_info": {
   "codemirror_mode": "text/x-scala",
   "file_extension": ".sc",
   "mimetype": "text/x-scala",
   "name": "scala",
   "nbconvert_exporter": "script",
   "version": "2.12.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
